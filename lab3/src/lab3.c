#include "lab3.h"
#include "mt19937.h"

/* ==================================================================== */
/* in_unit_circle                                                       */
/* Regarde si un point est dans le cercle unité                         */
/*                                                                      */
/* En entrée: x , y les coordonnées du point                            */
/*                                                                      */
/* En sortie: une estimation de la valeur de PI                         */
/* ==================================================================== */

int in_unit_circle(double x, double y)
{
    return (x * x + y * y) <= 1;
}

/* ==================================================================== */
/* simPi                                                                */
/* Génère des points pour trouver une estimation de pi                  */
/*                                                                      */
/* En entrée: nb_point nombre de point utilisé pour l'estimation        */
/*                                                                      */
/* En sortie: une estimation de la valeur de PI                         */
/* ==================================================================== */
double simPi(double nb_point)
{
    double x;
    double y;
    int point_inside = 0;

    for (int i = 0; i < nb_point; i++)
    {
        x = genrand_real1();
        y = genrand_real1();
        point_inside += in_unit_circle(x, y);
    }

    return 4 * (double)point_inside / nb_point;
}

/* ==================================================================== */
/* display_precision                                                    */
/* Affiche une estimation du nombre de point nécessaire pour atteindre  */
/* les degrés de précision                                              */
/*                                                                      */
/* En entrée: pas d'entrée                                              */
/*                                                                      */
/* En sortie: pas de sortie                                             */
/*                                                                      */
/* Effet de bord : Affichage du nombre de points necessaire pour        */
/*                 atteindre les précisions                             */
/* ==================================================================== */

void display_precision()
{
    int precision = 2;
    int i = 2;
    double simpi;
    while (precision < 5)
    {
        simpi = simPi(i);
        if (simpi >= M_PI - pow(0.1, precision) && simpi <= M_PI + pow(0.1, precision))
        {
            printf("Précision 10-%d atteinte au nb_point %d\n", precision, i);
            precision++;
        }
        i++;
    }
}

/* ==================================================================== */
/* sim_of_simPi                                                         */
/* Fait n simulations de la focntion simPi                              */
/*                                                                      */
/* En entrée: nb_point nombre de point utilisé pour l'estimation        */
/*            drawings nombre de simulations effectuees                 */
/*                                                                      */
/* En sortie: Le tableau de resultats                                   */
/*                                                                      */
/* Effet de bord : Affichage de la moyenne et des erreurs               */
/* ==================================================================== */

double *sim_of_simPi(int nb_point, int drawings)
{
    double *result = (double *)malloc(drawings * sizeof(double));
    for (int i = 0; i < drawings; i++)
    {
        result[i] = simPi(nb_point);
    }

    return result;
}

/* ==================================================================== */
/* mean_array                                                           */
/* calcule la moyenne                                                   */
/*                                                                      */
/* En entrée: result le tableau de resultats des simulations            */
/*            drawings nombre de simulations effectuees                 */
/*                                                                      */
/* En sortie: la moyenne                                                */
/* ==================================================================== */

double mean_array(double *result, int drawings)
{
    double mean = 0;

    for (int i = 0; i < drawings; i++)
    {
        mean += result[i];
    }

    return mean / drawings;
}

/* ==================================================================== */
/* display_mean_error                                                   */
/* affiche la moyenne et les erreurs par rapport a PI                   */
/*                                                                      */
/* En entrée: result le tableau de resultats des simulations            */
/*            drawings nombre de simulations effectuees                 */
/*                                                                      */
/* En sortie: pas de sortie                                             */
/*                                                                      */
/* Effet de bord : Affichage de la moyenne et des erreurs               */
/* ==================================================================== */

void display_mean_error(double *result, int drawings)
{
    double mean_pi, absolute_error, relative_error;

    mean_pi = mean_array(result, drawings);
    absolute_error = fabs(mean_pi - M_PI);
    relative_error = (absolute_error / M_PI);
    printf("Moyenne obtenue pour %d est de %f\n", drawings, mean_pi);
    printf("Erreur absolue : %f\n", absolute_error);
    printf("Erreur relative : %f\n", relative_error);
}

/* ==================================================================== */
/* variance_array                                                       */
/* calcule la variance                                                  */
/*                                                                      */
/* En entrée: result le tableau de resultats des simulations            */
/*            drawings nombre de simulations effectuees                 */
/*                                                                      */
/* En sortie: la variance                                               */
/* ==================================================================== */

double variance_array(double *result, int drawings)
{
    double mean = mean_array(result, drawings);
    double variance = 0;

    for (int i = 0; i < drawings; i++)
    {
        variance += (result[i] - mean) * (result[i] - mean);
    }

    return variance / (drawings - 1);
}

/* ==================================================================== */
/* confidence_interval_95                                               */
/* calcule l'intervalle de confiance a 95%                              */
/*                                                                      */
/* En entrée: result le tableau de resultats des simulations            */
/*            drawings nombre de simulations effectuees                 */
/*                                                                      */
/* En sortie: Le tableau de deux flottants correspondant a l'intervalle */
/*            en fonction de Student                                    */
/* ==================================================================== */

double *confidence_interval_95(double *result, int drawings)
{
    double student[30] = {12.706, 4.303, 3.182, 2.776, 2.571, 2.447, 2.365, 2.308, 2.262, 2.228,
                          2.201, 2.179, 2.160, 2.145, 2.131, 2.120, 2.110, 2.101, 2.093, 2.086,
                          2.080, 2.074, 2.069, 2.064, 2.060, 2.056, 2.052, 2.048, 2.045, 2.042};
    double *interval_of_confidence = (double *)calloc(2, sizeof(double));
    double mean = mean_array(result, drawings);
    double variance = variance_array(result, drawings);
    double t = 0;
    double ray_of_confidence;

    if (drawings <= 30)
    {
        t = student[drawings + 1];
    }
    else
    {
        if (drawings == 40)
            t = 2.021;
        else if (drawings == 80)
            t = 2.000;
        else if (drawings == 120)
            t = 1.980;
        else if (drawings == INT_MAX) // simule infini par la borne sup d'un int
            t = 1.960;
    }

    ray_of_confidence = t * sqrt(variance / drawings);
    interval_of_confidence[0] = mean - ray_of_confidence;
    interval_of_confidence[1] = mean + ray_of_confidence;
    return interval_of_confidence;
}

int main()
{
    // Initialisation de la séquence du  générateur
    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
    init_by_array(init, length);

    /*printf("================= Question 1 =================\n");
    printf("1 000 simPi = %lf\n", simPi(1000));
    printf("1 000 000 simPi = %lf\n", simPi(1000000));
    printf("1 000 000 000 simPi = %lf\n", simPi(1000000000));
    display_precision();
    printf("==============================================\n");*/

    /*printf("================= Question 2 =================\n");
    double *result = sim_of_simPi(1000, 40);
    display_mean_error(result, 40;
    result = sim_of_simPi(1000000, 40);
    display_mean_error(result, 40);
    result = sim_of_simPi(1000000000, 40);
    display_mean_error(result, 40);
    printf("==============================================\n");*/

    /*printf("================= Question 3 =================\n");
    double *result = sim_of_simPi(1000000, 10);
    double *intervalle = confidence_interval_95(result, 10);
    printf("[%f,%f]\n", intervalle[0], intervalle[1]);
    result = sim_of_simPi(1000000, 20);
    intervalle = confidence_interval_95(result, 20);
    printf("[%f,%f]\n", intervalle[0], intervalle[1]);
    result = sim_of_simPi(1000000, 40);
    intervalle = confidence_interval_95(result, 40);
    printf("[%f,%f]\n", intervalle[0], intervalle[1]);
    printf("==============================================\n");*/

    return 0;
}
