#ifndef LAB3
#define LAB3

#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#define _USE_MATH_DEFINES
#define INT_MAX 2147483647

int in_unit_circle(double x, double y);
double simPi(double nb_point);
void display_precision();

double *sim_of_simPi(int nb_point, int drawings);
double mean_array(double *result, int drawings);
void display_mean_error(double *result, int drawings);

double variance_array(double *result, int drawings);
double *confidence_interval_95(double *result, int drawings);

#endif