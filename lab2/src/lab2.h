#ifndef LAB2
#define LAB2

#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#define _USE_MATH_DEFINES

double uniform(double a, double b);

double divide(double value, int divide);
void map_divide(double *list, int lenght, int divide, double (*f)(double, int));
double *discrete_species_simulation(int nb_sim);

double *frequencies_table(double *list, int lenght);
double *species_simulation(int lenght, double *observation, int nb_sim);

double *cumulative_species_simulation(int lenght, double *observation, int nb_sim);

double neg_exp(double mean);
double average_neg_exp(int nb_tirages);
double *discrete_distribution_negExp(int nb_tirages);

int rolls40();
int *dice_rolls(int rolls);

void display_mean_variation(int *occurence, int lenght, int drawings, double first_value, double step);
int get_index(double x);
int *box_muller(int drawings);

#endif