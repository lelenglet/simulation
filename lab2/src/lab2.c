#include "mt19937.h"
#include "lab2.h"

/* ==================================================================== */
/* uniform                                                              */
/* Génère un nombre aléatoire uniformément distribué entre A et B       */
/*                                                                      */
/* En entrée: a, b deux nombres flottant                                */
/*                                                                      */
/* En sortie: La valeur flottante retournée est compris entre           */
/*            les bornes A et B                                         */
/* ==================================================================== */

double uniform(double a, double b)
{
    return a + (b - a) * genrand_real1();
}

/* ==================================================================== */
/* divide                                                               */
/* applique une division sur une valeur                                 */
/*                                                                      */
/* En entrée: value un double                                           */
/*            divide un entier                                          */
/*                                                                      */
/* En sortie: La valeur flottante retournée est le résultat de la       */
/*            division                                                  */
/* ==================================================================== */
double divide(double value, int divide)
{
    return value /= divide;
}

/* ==================================================================== */
/* map_divide                                                           */
/* applique une division à toutes les valeurs d'une liste               */
/*                                                                      */
/* En entrée : list tableau de flottants                                */
/*             lenght la longueur de la liste                           */
/*             divide un double                                         */
/*                                                                      */
/* En sortie: pas de sortie                                             */
/*                                                                      */
/* Effet de bord : Chaque élément de la liste est divisé par 'divide'   */
/* ==================================================================== */
void map_divide(double *list, int lenght, int divide, double (*f)(double, int))
{
    for (int i = 0; i < lenght; i++)
    {
        list[i] = f(list[i], divide);
    }
}

/* ==================================================================== */
/* discrete_species_simulation                                          */
/* simulation discrete de 3 classes de population A, B et C             */
/*                                                                      */
/* En entrée : nb_sim nombre de simulation                              */
/*                                                                      */
/* En sortie: Le tableau des fréquences de chaque espèce                */
/* ==================================================================== */

double *discrete_species_simulation(int nb_sim)
{
    int i;
    double *frequencies = (double *)calloc(3, sizeof(double));

    for (i = 0; i < nb_sim; i++)
    {
        double sim = genrand_real1();
        if (sim <= 0.35)
        {
            frequencies[0] += 1;
        }
        else if (sim > 0.35 && sim <= 0.80)
        {
            frequencies[1] += 1;
        }
        else
        {
            frequencies[2] += 1;
        }
    }
    map_divide(frequencies, 3, nb_sim, divide);
    return frequencies;
}

/* ==================================================================== */
/* frequencies_table                                                    */
/* transforme un tableau d'occurences en tableau de fréquences          */
/*                                                                      */
/* En entrée : list tableau de flottants                                */
/*             lenght la longueur de la liste                           */
/*                                                                      */
/* En sortie: list la liste des fréquences                              */
/* ==================================================================== */

double *frequencies_table(double *list, int lenght)
{
    int i;
    double total = 0;

    for (i = 0; i < lenght; i++)
    {
        total += list[i];
    }
    map_divide(list, lenght, total, divide);

    return list;
}

/* ==================================================================== */
/* species_simulation                                                   */
/* fait une simulation à partir d'un tableau d'observations             */
/*                                                                      */
/* En entrée : observation tableau de flottants                         */
/*             lenght la longueur de la liste                           */
/*             nb_sim nombre de simulation                              */
/*                                                                      */
/* En sortie: frequencies le tableau des fréquences obtenu en fonction  */
/*            des simulations                                           */
/* ==================================================================== */

double *species_simulation(int lenght, double *observation, int nb_sim)
{
    int i;
    observation = frequencies_table(observation, lenght);
    double *frequencies = (double *)calloc(lenght, sizeof(double));

    for (i = 0; i < nb_sim; i++)
    {
        double sim = genrand_real1();
        double freq = 0;
        for (int j = 0; j < lenght; j++)
        {
            if (sim <= freq + (observation[j]))
            {
                frequencies[j] += 1;
            }
            freq += observation[j];
        }
    }
    map_divide(frequencies, lenght, nb_sim, divide);
    return frequencies;
}

/* ==================================================================== */
/* cumulative_species_simulation                                        */
/* fait une simulation à partir d'un tableau d'observations cumulées    */
/*                                                                      */
/* En entrée : observation tableau de flottants                         */
/*             lenght la longueur de la liste                           */
/*             nb_sim nombre de simulation                              */
/*                                                                      */
/* En sortie: frequencies le tableau des fréquences obtenu en fonction  */
/*            des simulations                                           */
/* ==================================================================== */

double *cumulative_species_simulation(int lenght, double *observation, int nb_sim)
{
    int i;
    double *frequencies = (double *)calloc(size_of_array, sizeof(double));
    int nb_observation = 0;
    for (int i = 0; i < lenght; i++)
    {
        nb_observation += observation[i];
    }

    for (i = 0; i < nb_sim; i++)
    {
        double sim = genrand_real1();
        for (i = 0; i < lenght; i++)
        {
            if (sim <= observation[i] / nb_observation)
            {
                frequencies[i] += 1;
            }
        }
    }
    map_divide(frequencies, lenght, nb_sim, divide);
    return frequencies;
}

/* ==================================================================== */
/* neg_exp                                                              */
/* effectue une simulation de la distribution de l'exponentielle        */
/* négative                                                             */
/*                                                                      */
/* En entrée : mean la moyenne                                          */
/*                                                                      */
/* En sortie: un double                                                 */
/* ==================================================================== */

double neg_exp(double mean)
{
    return (-1) * mean * log(1 - genrand_real1());
}

/* ==================================================================== */
/* average_neg_exp                                                      */
/* calcule la moyenne des tirages de la fonction neg_exp                */
/*                                                                      */
/* En entrée : nb_tirages le nombre de tirages effectués                */
/*                                                                      */
/* En sortie: moyenne                                                   */
/* ==================================================================== */

double average_neg_exp(int nb_tirages)
{
    int i;
    double average = 0;

    for (i = 0; i < nb_tirages; i++)
    {
        average += neg_exp(10);
    }

    return average / nb_tirages;
}

/* ==================================================================== */
/* discrete_distribution_negExp                                         */
/* regarde la districution des tirages de la fonction neg_exp           */
/*                                                                      */
/* En entrée : nb_tirages le nombre de tirages à effectuer              */
/*                                                                      */
/* En sortie: une liste des fréquences                                  */
/* ==================================================================== */

double *discrete_distribution_negExp(int nb_tirages)
{
    int i;
    double *frequencies = (double *)calloc(21, sizeof(double));

    for (i = 0; i < nb_tirages; i++)
    {
        int valeur = (int)neg_exp(10);
        if (valeur > 20)
        {
            frequencies[20]++;
        }
        else
        {
            frequencies[valeur]++;
        }
    }

    return frequencies;
}

/* ==================================================================== */
/* rolls40                                                              */
/* effectue un jeu de 40 lancers de dés                                 */
/*                                                                      */
/* En entrée : pas d'arguments en entrée                                */
/*                                                                      */
/* En sortie: sum la somme des 40 lancers                               */
/* ==================================================================== */

int rolls40()
{
    int i;
    int sum = 0;

    for (i = 0; i < 40; i++)
    {
        sum += (int)uniform(1, 6);
    }

    return sum;
}

/* ==================================================================== */
/* dice_rolls                                                           */
/* simule un certain nombre de fois les 40 lancers de dés               */
/*                                                                      */
/* En entrée : rolls le nombre de simulation                            */
/*                                                                      */
/* En sortie: liste des occurences de somme                             */
/* ==================================================================== */

int *dice_rolls(int rolls)
{
    int i;
    int *occurence = (int *)calloc(201, sizeof(int));

    for (i = 0; i <= rolls; i++)
    {
        occurence[rolls40() - 40]++;
    }

    return occurence;
}

/* ==================================================================== */
/* display_mean_variation                                               */
/* calcule  la moyenne et l'écart type d'une liste d'occurences         */
/*                                                                      */
/* En entrée : occurence une liste                                      */
/*             lenght la longueur de la liste                           */
/*             drawings le nombre de tirages                            */
/*             first_value la première valeur des occurences possibles  */
/*             step le pas entre les valeurs                            */
/*                                                                      */
/* En sortie: pas de sortie                                             */
/*                                                                      */
/* Effet de bord : Affichage de la moyenne et de l'écart-type           */
/* ==================================================================== */

void display_mean_variation(int *occurence, int lenght, int drawings, double first_value, double step)
{
    double mean = 0;
    double standardDeviation = 0.0;
    double value = first_value;

    for (int i = 0; i < lenght; i++)
    {
        mean += (double)occurence[i] * (value);
        value += step;
    }
    mean = mean / (double)drawings;
    printf("Moyenne = %.2f\n", mean);

    value = first_value;
    for (int i = 0; i < lenght; i++)
    {
        standardDeviation += occurence[i] * pow((value - mean), 2);
        value += step;
    }
    printf("Ecart-type : %.2f\n", sqrt(standardDeviation / drawings));
}

/* ==================================================================== */
/* get_index                                                            */
/* récupère l'index du tableau d'occurence correspondant à x            */
/*                                                                      */
/* En entrée : x un nombre flottant généré                              */
/*                                                                      */
/* En sortie: l'index correspondant dans la liste des occurences        */
/* ==================================================================== */

int get_index(double x)
{
    int index = -1;

    if (x < -5)
    {
        index = 0;
    }
    else if (x > 5)
    {
        index = 19;
    }
    else
    {
        int comp = ((int)x == (int)round(x));
        index = ((int)x + 4) * 2 + 1 + comp;
    }
    return index;
}

/* ==================================================================== */
/* box_muller                                                           */
/* simule une distribution selon la méthode Box and Muller              */
/*                                                                      */
/* En entrée : drawings le nombre de simulation                         */
/*                                                                      */
/* En sortie: liste des occurences                                      */
/* ==================================================================== */

int *box_muller(int drawings)
{
    int *occurence = (int *)calloc(20, sizeof(int));
    for (int i = 0; i < drawings; i++)
    {
        double Rn1 = genrand_real1(), Rn2 = genrand_real1();
        double x1 = cos(2 * M_PI * Rn2) * sqrt(-2 * log2(Rn1));
        double x2 = sin(2 * M_PI * Rn2) * sqrt(-2 * log2(Rn1));

        occurence[get_index(x1)]++;
        occurence[get_index(x2)]++;
    }
    return occurence;
}

int main(void)
{
    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
    init_by_array(init, length);

    /*printf("================= Question 2 =================\n");
    printf("Temperature tirée : %f°C\n", uniform(-98, 57.7));
    printf("==============================================\n");

    printf("\n================= Question 3 a =================\n");
    printf("----------------- 1000 tirages -----------------\n");
    double *frequencies = discrete_species_simulation(1000);
    printf("A = %f%% B = %f%% C = %f%%\n", frequencies[0] * 100, frequencies[1] * 100, frequencies[2] * 100);
    printf("----------------- 10000 tirages -----------------\n");
    frequencies = discrete_species_simulation(10000);
    printf("A = %f%% B = %f%% C = %f%%\n", frequencies[0] * 100, frequencies[1] * 100, frequencies[2] * 100);
    printf("----------------- 100000 tirages -----------------\n");
    frequencies = discrete_species_simulation(100000);
    printf("A = %f%% B = %f%% C = %f%%\n", frequencies[0] * 100, frequencies[1] * 100, frequencies[2] * 100);
    printf("----------------- 1000000 tirages -----------------\n");
    frequencies = discrete_species_simulation(1000000);
    printf("A = %f%% B = %f%% C = %f%%\n", frequencies[0] * 100, frequencies[1] * 100, frequencies[2] * 100);
    printf("================================================\n");

    printf("\n================= Question 4 =================\n");
    printf("----------------- neg_exp b -----------------\n");
    printf("Moyenne de neg_exp pour 1 000 tirages : %.2f\n", average_neg_exp(1000));
    printf("Moyenne de neg_exp pour 1 000 000 tirages : %.2f\n", average_neg_exp(1000000));
    printf("----------------- neg_exp c 1k tirages -----------------\n");
    double *frequencies1k = discrete_distribution_negExp(1000);
    double *frequencies1m = discrete_distribution_negExp(1000000);
    for (int i = 0; i <= 20; i++)
    {
        if (i == 20)
        {
            printf("> 20 = %.0f\n", frequencies1k[i]);
        }
        else
        {
            printf("%d à %d : %.0f\n", i, i + 1, frequencies1k[i]);
        }
    }
    printf("----------------- neg_exp c 1m tirages -----------------\n");
    for (int i = 0; i <= 20; i++)
    {
        if (i == 20)
        {
            printf("> 20 = %.0f\n", frequencies1m[i]);
        }
        else
        {
            printf("%d à %d : %.0f\n", i, i + 1, frequencies1m[i]);
        }
    }
    printf("================================================\n");

    printf("\n================= Question 5 a =================\n");
    int *occurence = dice_rolls(1000000);
    for (int i = 0; i < 201; i++)
    {
        printf("%d = %d |", i + 40, occurence[i]);
    }
    printf("\n");
    display_mean_variation(occurence, 201, 1000000, 40, 1);
    printf("================================================\n");

    printf("\n================= Question 5 b =================\n");
    occurence = box_muller(1000000);
    printf("< -5 = %d\n", occurence[0]);
    for (int i = 1; i < 19; i++)
    {
        // printf("%f à %f = %d\n", -5 + (i - 1) * 0.5, -5 + i * 0.5, occurence[i]);
        printf("%d\n", occurence[i]);
    }
    printf("> 5 = %d", occurence[19]);
    printf("\n");
    display_mean_variation(occurence, 20, 1000000, -5, 0.5);
    printf("================================================\n");*/

    return 0;
}
