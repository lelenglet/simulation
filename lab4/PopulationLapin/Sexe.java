public enum Sexe {
    MALE(0),
    FEMALE(1);

    private final int code;

    /**
     * Constructeur de l'enumeration Sexe.
     * 
     * @param code valeur correspondant à un sexe
     */
    Sexe(int code) {
        this.code = code;
    }

    /**
     * Accesseur de l'attribut code.
     * 
     * @return code : le code associé au sexe
     */
    public int getCode() {
        return code;
    }

    /**
     * Méthode statique permettant d'associer un sexe à une valeur entière.
     * 
     * @param code un entier (0,1)
     * @return sexe : le sexe associé au code passé en paramètre
     */
    public static Sexe fromInt(int code) {
        for (Sexe s : Sexe.values()) {
            if (s.getCode() == code) {
                return s;
            }
        }
        return Sexe.MALE;
    }
}
