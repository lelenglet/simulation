import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.apache.commons.math3.random.MersenneTwister;

public class Lapin {
    private EtatLapin etat;
    private int age;
    private Sexe sexe;
    private int nbportee;
    private int maturite;
    static public double mortaliteInfantile = 1 - 0.35;
    static public double mortaliteAdulte = 1 - 0.60;
    static public MersenneTwister mt = new MersenneTwister(2);

    /**
     * Constructeur permettant de créer une instance d'un nouveau né lors d'une
     * simulation.
     */
    public Lapin() {
        this.etat = new Jeune();
        this.age = 0;
        this.sexe = Sexe.fromInt(mt.nextInt(2));
        this.nbportee = 0;
        this.maturite = mt.nextInt(8 - 5 + 1) + 5;
    }

    /**
     * Constructeur permettant de créer une instance d'un adulte en vue de la
     * préparation de
     * la situation initiale de la simulation.
     * 
     * @param sexe : entier permettant de renseigner le sexe que l'adulte aura.
     */
    public Lapin(int sexe) {
        this.etat = new Mature();
        this.sexe = Sexe.fromInt(sexe);
        if (this.sexe == Sexe.FEMALE) {
            this.age = 12;
            this.setNbportee();
        }
        this.age = 13;
        this.maturite = 0;
    }

    /**
     * Accesseur de l'attribut sexe.
     * 
     * @return Sexe : le sexe du lapin
     */
    public Sexe getSexe() {
        return this.sexe;
    }

    /**
     * Accesseur de l'attribut état.
     * 
     * @return EtatLapin : l'état actuel du lapin
     */
    public EtatLapin getEtat() {
        return this.etat;
    }

    /**
     * Modifie le taux de mortalité infantile.
     * 
     * @param m : nouvelle valeur
     */
    static public void setMortaliteInfantile(double m) {
        mortaliteInfantile = m;
    }

    /**
     * Modifie le taux de mortalité d'un adulte.
     * 
     * @param m : nouvelle valeur
     */
    static public void setMortaliteAdulte(double m) {
        mortaliteAdulte = m;
    }

    /**
     * Méthode représentant le cycle d'un mois de vie du lapin.
     * 
     * @param homme : booléen permettant de savoir si un male adulte est dans la
     *              population
     * 
     * @return List<Lapin> : une liste contenant les lapins nés et le lapin adulte
     *         observé
     */
    public List<Lapin> prochainMois(boolean homme) {
        List<Lapin> nouveauxLapins = new ArrayList<Lapin>();
        if (this.sexe == Sexe.FEMALE && homme == true && this.nbportee > 0) {
            List<Lapin> nouveauxNes = this.engendre();
            nouveauxLapins.addAll(nouveauxNes);
        }
        Lapin lapinExistant = this.evolue();
        if (lapinExistant != null)
            nouveauxLapins.add(lapinExistant);
        return nouveauxLapins;
    }

    /**
     * Méthode montrant l'augmentation de l'age d'un lapin.
     * 
     * @return Lapin : le lapin plus agé ou null si il meure
     */
    public Lapin evolue() {
        if (this.age % 12 == 0 && this.isDying() == true) {
            return null;
        }
        this.age++;
        if (this.age == this.maturite) {
            this.etat = etat.changeEtat();
        }
        if (this.sexe == Sexe.FEMALE && this.age >= this.maturite) {
            this.setNbportee();
        }
        return this;
    }

    /**
     * Méthode permettant de définir le nombre de portées que fera une femelle dans
     * cette année.
     * 
     * On applique une gaussienne centrée en 6 et élargie pour avoir plus de
     * probabilités d'obtenir un 5, 6 ou 7.
     */
    private void setNbportee() {
        if ((this.age - this.maturite) % 12 == 0) {
            double val;
            do {
                val = 6 + mt.nextGaussian() * 1.5;
            } while (val < 3 || val > 9);
            this.nbportee = (int) Math.round(val);
        }
    }

    /**
     * Méthode permettant de simuler la mort d'un lapin selon son age et son état.
     * 
     * @return booléen : indique si le lapin meurt
     */
    public boolean isDying() {
        double luck = mt.nextDouble();
        if ((this.etat instanceof Jeune && luck <= 1 - mortaliteInfantile)
                || (this.etat instanceof Mature && luck <= 1 - mortaliteAdulte)
                || (this.age > 120 && luck <= 0.60 - 0.10 - 0.1 * ((this.age - 120) / 12))) {
            return false;
        }
        return true;
    }

    /**
     * Méthode qui fait naître les laperaux.
     * 
     * @return List<Lapin> : liste des nouveaux nés
     */
    public List<Lapin> engendre() {
        int nbLapin = 0;
        List<Lapin> nouveauxNes = new ArrayList<Lapin>();
        if ((12 - ((this.age - this.maturite) % 12) == this.nbportee) || (mt.nextDouble() <= 0.5)) {
            nbLapin = mt.nextInt(6 - 3 + 1) + 3;
            this.nbportee--;
        }
        for (int i = 0; i < nbLapin; i++) {
            nouveauxNes.add(new Lapin());
        }
        return nouveauxNes;
    }

    @Override
    public String toString() {
        String s;
        if (this.sexe == Sexe.FEMALE) {
            if (this.etat instanceof Mature) {
                s = "F";
            } else {
                s = "f";
            }
        } else {
            if (this.etat instanceof Mature) {
                s = "M";
            } else {
                s = "m";
            }
        }
        return s;
    }

}
