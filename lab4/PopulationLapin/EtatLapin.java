public interface EtatLapin {

    /**
     * Méthode permettant de modifier son état.
     * 
     * @return etat : le nouvel état du lapin
     */
    EtatLapin changeEtat();

    /**
     * Méthode permettant l'affichage de l'état d'un lapin.
     * 
     * @return string : la chaine de caractère à afficher
     */
    String toString();
}
