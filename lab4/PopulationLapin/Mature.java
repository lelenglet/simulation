public class Mature implements EtatLapin {

    public EtatLapin changeEtat() {
        return new Mature();
    }

    @Override
    public String toString() {
        return "mature";
    }
}
