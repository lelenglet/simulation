import java.util.ArrayList;
import java.util.List;

public class Monde {
    private List<Lapin> population;

    /**
     * Constructeur de notre simulateur.
     * 
     * Génère la liste qui contiendra notre population de lapin (population).
     */
    public Monde() {
        this.population = new ArrayList<Lapin>();
    }

    /**
     * Méthode qui représente un cycle de notre simulation.
     * 
     * Modifie l'état de la population pour chaque mois.
     */
    public void prochainMois() {
        List<Lapin> nouveauxLapins = new ArrayList<Lapin>();
        for (Lapin lapin : this.population) {
            List<Lapin> nouveauxNes = lapin.prochainMois(this.hasMale());
            nouveauxLapins.addAll(nouveauxNes);
        }
        this.population = nouveauxLapins;
    }

    /**
     * Méthode permettant de savoir si un male adulte est présent dans la
     * population.
     * 
     * @return homme : booléen qui indique si il y a un male
     */
    public boolean hasMale() {
        boolean homme = false;
        int i = 0;
        while (!homme && i < this.population.size()) {
            if (this.population.get(i).getSexe() == Sexe.MALE && this.population.get(i).getEtat() instanceof Mature) {
                homme = true;
            }
            i++;
        }
        return homme;
    }

    @Override
    public String toString() {
        String s = ""+this.population.size();
        /*
         * for (Lapin l : this.population) {
         * s += l.toString();
         * }
         */
        return s;
    }

    /**
     * Méthode qui initialise la population pour la première observation.
     */
    public void situation1() {
        this.population = new ArrayList<Lapin>();
        for (int i = 0; i < 1; i++) {
            this.population.add(new Lapin(0));
            this.population.add(new Lapin(1));
        }
    }

    /**
     * Méthode qui initialise la population pour la seconde observation.
     */
    public void situation5() {
        this.population = new ArrayList<Lapin>();
        for (int i = 0; i < 5; i++) {
            this.population.add(new Lapin(0));
            this.population.add(new Lapin(1));
        }
    }

    /**
     * Méthode qui initialise la population pour la troisième observation.
     */
    public void situation10() {
        this.population = new ArrayList<Lapin>();
        for (int i = 0; i < 10; i++) {
            this.population.add(new Lapin(0));
            this.population.add(new Lapin(1));
        }
    }

    /**
     * Méthode permettant de faire écouler N années dans la simulation.
     * 
     * @param nbAnnee       : nombre d'années qui passe
     * @param anneeEpidemie : année ou survient la maladie (-1 si pas de maladie)
     * @param mortEnfant    : valeur de la mortalité infantile avec la maladie
     * @param mortAdulte    : valeur de la mortalité adulte avec la maladie
     */
    public void simulateur(int nbAnnee, int anneeEpidemie, double mortEnfant, double mortAdulte) {
        this.epidemie(1-0.35,1-0.60);
        for (int i = 0; i < nbAnnee * 12; i++) {
            if (i == anneeEpidemie * 12) {
                epidemie(mortEnfant, mortAdulte);
            }
            System.out.println(this.toString());
            this.prochainMois();
        }
    }

    /**
     * Modifie les taux de mortalité suite à l'apparition d'une maladie au sein de
     * la population.
     * 
     * @param mortEnfant : valeur de la mortalité infantile avec la maladie
     * @param mortAdulte : valeur de la mortalité adulte avec la maladie
     */
    public void epidemie(double mortEnfant, double mortAdulte) {
        Lapin.setMortaliteAdulte(mortAdulte);
        Lapin.setMortaliteInfantile(mortEnfant);
    }

    public static void main(String[] argv) {
        Monde pop = new Monde();
        System.out.println("========================= SITUATION 1M 1F =========================");
        pop.situation10();
        pop.simulateur(5, -1, 0.85, 0.70);

        System.out.println("========================= SITUATION 1M 1F MALADIE1 =========================");
        pop.situation10();
        pop.simulateur(5, 2, 1-0.35 +0.20, 1-0.60 +0.20);

        System.out.println("========================= SITUATION 1M 1F MALADIE2 =========================");
        pop.situation10();
        pop.simulateur(5, 2, 1-0.35, 1-0.60+0.30);

        System.out.println("========================= SITUATION 1M 1F MALADIE3 =========================");
        pop.situation10();
        pop.simulateur(5, 2, 1 - 0.35 +0.30, 1 - 0.60);

        /*System.out.println("========================= SITUATION 5M 5F =========================");
        pop.situation5();
        pop.simulateur(10, 5, 0.85, 0.55);

        System.out.println("======================== SITUATION 10M 10F ========================");
        pop.situation10();
        pop.simulateur(10, 5, 0.85, 0.55);*/
    }
}
