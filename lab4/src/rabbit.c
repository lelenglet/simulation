#include <stdio.h>

void display_simulation_rabbit(int initial_situation, int months)
{
    int rabbit_old = initial_situation;
    int new_born = 0;
    int rabbit_young = 0;

    for (int i = 1; i <= months; i++)
    {
        for (int j = 0; j < rabbit_old; j++)
        {
            printf("()");
            new_born++;
            printf("o");
        }
        rabbit_old += rabbit_young;
        for (int j = 0; j < rabbit_young; j++)
        {
            printf("()");
        }
        rabbit_young = 0;
        rabbit_young += new_born;
        new_born = 0;
        printf("\n");
    }
    printf("Population = %d couples de lapins", rabbit_old + rabbit_young);
}

int main()
{
    display_simulation_rabbit(2, 10);
    return 0;
}